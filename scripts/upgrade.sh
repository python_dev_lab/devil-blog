#!/bin/bash
docker stop devil-blog_PRODUCTION
docker rm devil-blog_PRODUCTION
docker run -d --name devil-blog_PRODUCTION -e TARGET_THEME=devil-theme -e NODE_ENV=production -p 80:2368 -v /usr/share/ghost-blogs/devil-blog:/var/lib/ghost devil-blog:0.1.0
