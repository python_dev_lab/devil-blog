#!/bin/bash
docker stop devil-blog_DEVELOPMENT
docker rm devil-blog_DEVELOPMENT
docker run -d --name devil-blog_DEVELOPMENT -e NODE_ENV=development -p 2368:2368 -v `pwd`:/var/lib/ghost devil-blog:0.1.0
